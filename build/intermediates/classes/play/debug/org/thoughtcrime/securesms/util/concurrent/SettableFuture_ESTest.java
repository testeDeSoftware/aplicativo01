/*
 * This file was automatically generated by EvoSuite
 * Thu Jul 20 04:12:07 GMT 2017
 */

package org.thoughtcrime.securesms.util.concurrent;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.shaded.org.mockito.Mockito.*;
import static org.evosuite.runtime.MockitoExtension.*;
import static org.evosuite.runtime.EvoAssertions.*;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.evosuite.runtime.ViolatedAssumptionAnswer;
import org.evosuite.runtime.mock.java.io.MockPrintWriter;
import org.evosuite.runtime.mock.java.lang.MockThrowable;
import org.junit.runner.RunWith;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class SettableFuture_ESTest extends SettableFuture_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test00()  throws Throwable  {
      SettableFuture<Object> settableFuture0 = new SettableFuture<Object>();
      assertNotNull(settableFuture0);
      assertFalse(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      
      Object object0 = new Object();
      assertNotNull(object0);
      
      boolean boolean0 = settableFuture0.set(object0);
      assertFalse(settableFuture0.isCancelled());
      assertTrue(settableFuture0.isDone());
      assertTrue(boolean0);
      
      MockThrowable mockThrowable0 = new MockThrowable("Z`2I#(3DO'cqF8m4?H", (Throwable) null);
      assertNotNull(mockThrowable0);
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: Z`2I#(3DO'cqF8m4?H", mockThrowable0.toString());
      assertEquals("Z`2I#(3DO'cqF8m4?H", mockThrowable0.getMessage());
      
      StackTraceElement stackTraceElement0 = new StackTraceElement("Z`2I#(3DO'cqF8m4?H", "", "SAO", (-1666));
      assertNotNull(stackTraceElement0);
      assertEquals("SAO", stackTraceElement0.getFileName());
      assertEquals("Z`2I#(3DO'cqF8m4?H.(SAO)", stackTraceElement0.toString());
      assertFalse(stackTraceElement0.isNativeMethod());
      assertEquals((-1666), stackTraceElement0.getLineNumber());
      assertEquals("", stackTraceElement0.getMethodName());
      assertEquals("Z`2I#(3DO'cqF8m4?H", stackTraceElement0.getClassName());
      
      mockThrowable0.setOriginForDelegate(stackTraceElement0);
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: Z`2I#(3DO'cqF8m4?H", mockThrowable0.toString());
      assertEquals("Z`2I#(3DO'cqF8m4?H", mockThrowable0.getMessage());
      assertEquals("SAO", stackTraceElement0.getFileName());
      assertEquals("Z`2I#(3DO'cqF8m4?H.(SAO)", stackTraceElement0.toString());
      assertFalse(stackTraceElement0.isNativeMethod());
      assertEquals((-1666), stackTraceElement0.getLineNumber());
      assertEquals("", stackTraceElement0.getMethodName());
      assertEquals("Z`2I#(3DO'cqF8m4?H", stackTraceElement0.getClassName());
      
      StackTraceElement[] stackTraceElementArray0 = MockThrowable.replacement_getStackTrace(mockThrowable0);
      assertNotNull(stackTraceElementArray0);
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: Z`2I#(3DO'cqF8m4?H", mockThrowable0.toString());
      assertEquals("Z`2I#(3DO'cqF8m4?H", mockThrowable0.getMessage());
      
      boolean boolean1 = settableFuture0.setException(mockThrowable0);
      assertFalse(boolean1 == boolean0);
      assertFalse(settableFuture0.isCancelled());
      assertTrue(settableFuture0.isDone());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: Z`2I#(3DO'cqF8m4?H", mockThrowable0.toString());
      assertEquals("Z`2I#(3DO'cqF8m4?H", mockThrowable0.getMessage());
      assertFalse(boolean1);
      
      ListenableFuture.Listener<Object> listenableFuture_Listener0 = (ListenableFuture.Listener<Object>) mock(ListenableFuture.Listener.class, new ViolatedAssumptionAnswer());
      settableFuture0.addListener(listenableFuture_Listener0);
      assertFalse(settableFuture0.isCancelled());
      assertTrue(settableFuture0.isDone());
      
      SettableFuture<Integer> settableFuture1 = new SettableFuture<Integer>();
      assertNotNull(settableFuture1);
      assertFalse(settableFuture1.isCancelled());
      assertFalse(settableFuture1.isDone());
      
      Integer integer0 = new Integer(0);
      assertNotNull(integer0);
      assertEquals(0, (int)integer0);
      
      boolean boolean2 = settableFuture1.set(integer0);
      assertTrue(boolean2 == boolean0);
      assertFalse(boolean2 == boolean1);
      assertTrue(settableFuture1.isDone());
      assertFalse(settableFuture1.isCancelled());
      assertTrue(boolean2);
      
      boolean boolean3 = settableFuture0.setException((Throwable) null);
      assertTrue(boolean3 == boolean1);
      assertFalse(boolean3 == boolean2);
      assertFalse(boolean3 == boolean0);
      assertFalse(settableFuture0.isCancelled());
      assertTrue(settableFuture0.isDone());
      assertFalse(boolean3);
      
      SettableFuture<String> settableFuture2 = new SettableFuture<String>();
      assertNotNull(settableFuture2);
      assertFalse(settableFuture2.isCancelled());
      assertFalse(settableFuture2.isDone());
      
      boolean boolean4 = settableFuture2.setException(mockThrowable0);
      assertFalse(boolean4 == boolean3);
      assertTrue(boolean4 == boolean2);
      assertFalse(boolean4 == boolean1);
      assertTrue(boolean4 == boolean0);
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: Z`2I#(3DO'cqF8m4?H", mockThrowable0.toString());
      assertEquals("Z`2I#(3DO'cqF8m4?H", mockThrowable0.getMessage());
      assertTrue(settableFuture2.isDone());
      assertFalse(settableFuture2.isCancelled());
      assertTrue(boolean4);
  }

  @Test(timeout = 4000)
  public void test01()  throws Throwable  {
      SettableFuture<Object> settableFuture0 = new SettableFuture<Object>();
      assertNotNull(settableFuture0);
      assertFalse(settableFuture0.isCancelled());
      assertFalse(settableFuture0.isDone());
      
      Object object0 = new Object();
      assertNotNull(object0);
      
      boolean boolean0 = settableFuture0.set(object0);
      assertTrue(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      assertTrue(boolean0);
      
      boolean boolean1 = settableFuture0.isCancelled();
      assertFalse(boolean1 == boolean0);
      assertTrue(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      assertFalse(boolean1);
      
      SettableFuture<Integer> settableFuture1 = new SettableFuture<Integer>();
      assertNotNull(settableFuture1);
      assertFalse(settableFuture1.isCancelled());
      assertFalse(settableFuture1.isDone());
      
      ListenableFuture.Listener<Integer> listenableFuture_Listener0 = (ListenableFuture.Listener<Integer>) mock(ListenableFuture.Listener.class, new ViolatedAssumptionAnswer());
      settableFuture1.addListener(listenableFuture_Listener0);
      assertFalse(settableFuture1.isCancelled());
      assertFalse(settableFuture1.isDone());
      
      TimeUnit timeUnit0 = TimeUnit.HOURS;
      Object object1 = settableFuture0.get(0L, timeUnit0);
      assertNotNull(object1);
      assertSame(object1, object0);
      assertTrue(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      
      MockThrowable mockThrowable0 = new MockThrowable("");
      assertNotNull(mockThrowable0);
      assertEquals("", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable0.toString());
      
      StackTraceElement[] stackTraceElementArray0 = MockThrowable.replacement_getStackTrace(mockThrowable0);
      assertNotNull(stackTraceElementArray0);
      assertEquals("", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable0.toString());
      
      boolean boolean2 = settableFuture1.setException(mockThrowable0);
      assertFalse(boolean2 == boolean1);
      assertTrue(boolean2 == boolean0);
      assertTrue(settableFuture1.isDone());
      assertFalse(settableFuture1.isCancelled());
      assertEquals("", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable0.toString());
      assertTrue(boolean2);
      
      StackTraceElement[] stackTraceElementArray1 = MockThrowable.getDefaultStackTrace();
      assertFalse(stackTraceElementArray1.equals((Object)stackTraceElementArray0));
      assertNotNull(stackTraceElementArray1);
      assertNotSame(stackTraceElementArray1, stackTraceElementArray0);
      
      Integer integer0 = new Integer(0);
      assertNotNull(integer0);
      assertEquals(0, (int)integer0);
      
      boolean boolean3 = settableFuture1.set(integer0);
      assertTrue(boolean3 == boolean1);
      assertFalse(boolean3 == boolean2);
      assertFalse(boolean3 == boolean0);
      assertTrue(settableFuture1.isDone());
      assertFalse(settableFuture1.isCancelled());
      assertFalse(boolean3);
      
      boolean boolean4 = settableFuture1.set((Integer) null);
      assertFalse(boolean4 == boolean2);
      assertTrue(boolean4 == boolean1);
      assertFalse(boolean4 == boolean0);
      assertTrue(boolean4 == boolean3);
      assertTrue(settableFuture1.isDone());
      assertFalse(settableFuture1.isCancelled());
      assertFalse(boolean4);
      
      boolean boolean5 = settableFuture0.setException(mockThrowable0);
      assertTrue(boolean5 == boolean1);
      assertFalse(boolean5 == boolean0);
      assertFalse(boolean5 == boolean2);
      assertTrue(boolean5 == boolean3);
      assertTrue(boolean5 == boolean4);
      assertTrue(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      assertEquals("", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable0.toString());
      assertFalse(boolean5);
      
      SettableFuture<String> settableFuture2 = new SettableFuture<String>();
      assertNotNull(settableFuture2);
      assertFalse(settableFuture2.isCancelled());
      assertFalse(settableFuture2.isDone());
      
      boolean boolean6 = settableFuture2.cancel(true);
      assertFalse(boolean6 == boolean3);
      assertFalse(boolean6 == boolean4);
      assertTrue(boolean6 == boolean2);
      assertTrue(boolean6 == boolean0);
      assertFalse(boolean6 == boolean1);
      assertFalse(boolean6 == boolean5);
      assertTrue(settableFuture2.isCancelled());
      assertFalse(settableFuture2.isDone());
      assertTrue(boolean6);
      
      boolean boolean7 = settableFuture2.isCancelled();
      assertTrue(boolean7 == boolean0);
      assertFalse(boolean7 == boolean1);
      assertFalse(boolean7 == boolean5);
      assertTrue(boolean7 == boolean6);
      assertFalse(boolean7 == boolean3);
      assertFalse(boolean7 == boolean4);
      assertTrue(boolean7 == boolean2);
      assertTrue(settableFuture2.isCancelled());
      assertFalse(settableFuture2.isDone());
      assertTrue(boolean7);
      
      boolean boolean8 = settableFuture0.isCancelled();
      assertTrue(boolean8 == boolean3);
      assertTrue(boolean8 == boolean5);
      assertFalse(boolean8 == boolean0);
      assertFalse(boolean8 == boolean6);
      assertTrue(boolean8 == boolean1);
      assertFalse(boolean8 == boolean2);
      assertTrue(boolean8 == boolean4);
      assertFalse(boolean8 == boolean7);
      assertTrue(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      assertFalse(boolean8);
      
      boolean boolean9 = settableFuture0.setException(mockThrowable0);
      assertTrue(boolean9 == boolean4);
      assertTrue(boolean9 == boolean5);
      assertFalse(boolean9 == boolean7);
      assertTrue(boolean9 == boolean8);
      assertFalse(boolean9 == boolean6);
      assertFalse(boolean9 == boolean0);
      assertFalse(boolean9 == boolean2);
      assertTrue(boolean9 == boolean3);
      assertTrue(boolean9 == boolean1);
      assertTrue(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      assertEquals("", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable0.toString());
      assertFalse(boolean9);
      
      try { 
        settableFuture1.get();
        fail("Expecting exception: ExecutionException");
      
      } catch(ExecutionException e) {
         //
         // org.evosuite.runtime.mock.java.lang.MockThrowable: 
         //
         verifyException("org.thoughtcrime.securesms.util.concurrent.SettableFuture", e);
      }
  }

  @Test(timeout = 4000)
  public void test02()  throws Throwable  {
      SettableFuture<Object> settableFuture0 = new SettableFuture<Object>();
      assertNotNull(settableFuture0);
      assertFalse(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      
      ListenableFuture.Listener<Object> listenableFuture_Listener0 = (ListenableFuture.Listener<Object>) mock(ListenableFuture.Listener.class, new ViolatedAssumptionAnswer());
      settableFuture0.addListener(listenableFuture_Listener0);
      assertFalse(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      
      TimeUnit timeUnit0 = TimeUnit.MINUTES;
      // Undeclared exception!
      try { 
        settableFuture0.get((-1L), timeUnit0);
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // timeout value is negative
         //
         verifyException("java.lang.Object", e);
      }
  }

  @Test(timeout = 4000)
  public void test03()  throws Throwable  {
      SettableFuture<Object> settableFuture0 = new SettableFuture<Object>();
      assertNotNull(settableFuture0);
      assertFalse(settableFuture0.isCancelled());
      assertFalse(settableFuture0.isDone());
      
      Object object0 = new Object();
      assertNotNull(object0);
      
      boolean boolean0 = settableFuture0.set(object0);
      assertFalse(settableFuture0.isCancelled());
      assertTrue(settableFuture0.isDone());
      assertTrue(boolean0);
      
      SettableFuture<String> settableFuture1 = new SettableFuture<String>();
      assertNotNull(settableFuture1);
      assertFalse(settableFuture1.isCancelled());
      assertFalse(settableFuture1.isDone());
      
      ListenableFuture.Listener<String> listenableFuture_Listener0 = (ListenableFuture.Listener<String>) mock(ListenableFuture.Listener.class, new ViolatedAssumptionAnswer());
      settableFuture1.addListener(listenableFuture_Listener0);
      assertFalse(settableFuture1.isCancelled());
      assertFalse(settableFuture1.isDone());
  }

  @Test(timeout = 4000)
  public void test04()  throws Throwable  {
      SettableFuture<Object> settableFuture0 = new SettableFuture<Object>();
      assertNotNull(settableFuture0);
      assertFalse(settableFuture0.isCancelled());
      assertFalse(settableFuture0.isDone());
      
      MockThrowable mockThrowable0 = new MockThrowable();
      assertNotNull(mockThrowable0);
      assertNull(mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable0.toString());
      
      boolean boolean0 = settableFuture0.setException(mockThrowable0);
      assertFalse(settableFuture0.isCancelled());
      assertTrue(settableFuture0.isDone());
      assertNull(mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable0.toString());
      assertTrue(boolean0);
      
      SettableFuture<String> settableFuture1 = new SettableFuture<String>();
      assertNotNull(settableFuture1);
      assertFalse(settableFuture1.isCancelled());
      assertFalse(settableFuture1.isDone());
      
      boolean boolean1 = settableFuture0.setException(mockThrowable0);
      assertFalse(boolean1 == boolean0);
      assertFalse(settableFuture0.isCancelled());
      assertTrue(settableFuture0.isDone());
      assertNull(mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable0.toString());
      assertFalse(boolean1);
      
      MockThrowable mockThrowable1 = new MockThrowable("");
      assertFalse(mockThrowable1.equals((Object)mockThrowable0));
      assertNotNull(mockThrowable1);
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable1.toString());
      assertEquals("", mockThrowable1.getMessage());
      
      mockThrowable0.addSuppressed(mockThrowable1);
      assertFalse(mockThrowable0.equals((Object)mockThrowable1));
      assertFalse(mockThrowable1.equals((Object)mockThrowable0));
      assertNotSame(mockThrowable0, mockThrowable1);
      assertNotSame(mockThrowable1, mockThrowable0);
      assertNull(mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable0.toString());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable1.toString());
      assertEquals("", mockThrowable1.getMessage());
      
      Object object0 = new Object();
      assertNotNull(object0);
      
      boolean boolean2 = settableFuture0.set(object0);
      assertTrue(boolean2 == boolean1);
      assertFalse(boolean2 == boolean0);
      assertFalse(settableFuture0.isCancelled());
      assertTrue(settableFuture0.isDone());
      assertFalse(boolean2);
      
      boolean boolean3 = settableFuture1.cancel(true);
      assertFalse(boolean3 == boolean1);
      assertTrue(boolean3 == boolean0);
      assertFalse(boolean3 == boolean2);
      assertTrue(settableFuture1.isCancelled());
      assertFalse(settableFuture1.isDone());
      assertTrue(boolean3);
      
      boolean boolean4 = settableFuture1.setException(mockThrowable0);
      assertFalse(mockThrowable0.equals((Object)mockThrowable1));
      assertFalse(boolean4 == boolean0);
      assertTrue(boolean4 == boolean2);
      assertFalse(boolean4 == boolean3);
      assertTrue(boolean4 == boolean1);
      assertNotSame(mockThrowable0, mockThrowable1);
      assertNull(mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable0.toString());
      assertTrue(settableFuture1.isCancelled());
      assertFalse(settableFuture1.isDone());
      assertFalse(boolean4);
      
      boolean boolean5 = settableFuture1.isCancelled();
      assertTrue(boolean5 == boolean3);
      assertFalse(boolean5 == boolean2);
      assertFalse(boolean5 == boolean4);
      assertTrue(boolean5 == boolean0);
      assertFalse(boolean5 == boolean1);
      assertTrue(settableFuture1.isCancelled());
      assertFalse(settableFuture1.isDone());
      assertTrue(boolean5);
      
      boolean boolean6 = settableFuture1.isDone();
      assertFalse(boolean6 == boolean3);
      assertTrue(boolean6 == boolean2);
      assertTrue(boolean6 == boolean4);
      assertFalse(boolean6 == boolean5);
      assertFalse(boolean6 == boolean0);
      assertTrue(boolean6 == boolean1);
      assertTrue(settableFuture1.isCancelled());
      assertFalse(settableFuture1.isDone());
      assertFalse(boolean6);
      
      boolean boolean7 = settableFuture1.setException(mockThrowable0);
      assertFalse(mockThrowable0.equals((Object)mockThrowable1));
      assertTrue(boolean7 == boolean6);
      assertTrue(boolean7 == boolean2);
      assertTrue(boolean7 == boolean1);
      assertFalse(boolean7 == boolean0);
      assertTrue(boolean7 == boolean4);
      assertFalse(boolean7 == boolean5);
      assertFalse(boolean7 == boolean3);
      assertNotSame(mockThrowable0, mockThrowable1);
      assertNull(mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable0.toString());
      assertTrue(settableFuture1.isCancelled());
      assertFalse(settableFuture1.isDone());
      assertFalse(boolean7);
      
      boolean boolean8 = settableFuture0.isDone();
      assertFalse(boolean8 == boolean4);
      assertTrue(boolean8 == boolean5);
      assertFalse(boolean8 == boolean7);
      assertFalse(boolean8 == boolean1);
      assertTrue(boolean8 == boolean0);
      assertFalse(boolean8 == boolean2);
      assertTrue(boolean8 == boolean3);
      assertFalse(boolean8 == boolean6);
      assertFalse(settableFuture0.isCancelled());
      assertTrue(settableFuture0.isDone());
      assertTrue(boolean8);
      
      TimeUnit timeUnit0 = TimeUnit.HOURS;
      try { 
        settableFuture1.get(0L, timeUnit0);
        fail("Expecting exception: TimeoutException");
      
      } catch(TimeoutException e) {
         //
         // no message in exception (getMessage() returned null)
         //
         verifyException("org.thoughtcrime.securesms.util.concurrent.SettableFuture", e);
      }
  }

  @Test(timeout = 4000)
  public void test05()  throws Throwable  {
      SettableFuture<Integer> settableFuture0 = new SettableFuture<Integer>();
      assertNotNull(settableFuture0);
      assertFalse(settableFuture0.isCancelled());
      assertFalse(settableFuture0.isDone());
      
      MockThrowable mockThrowable0 = new MockThrowable("7(MpYS%InqI?=h7a");
      assertNotNull(mockThrowable0);
      assertEquals("7(MpYS%InqI?=h7a", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable0.toString());
      
      MockThrowable mockThrowable1 = new MockThrowable(mockThrowable0);
      assertFalse(mockThrowable1.equals((Object)mockThrowable0));
      assertNotNull(mockThrowable1);
      assertEquals("7(MpYS%InqI?=h7a", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable0.toString());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable1.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable1.toString());
      
      boolean boolean0 = settableFuture0.setException(mockThrowable1);
      assertFalse(mockThrowable0.equals((Object)mockThrowable1));
      assertFalse(mockThrowable1.equals((Object)mockThrowable0));
      assertNotSame(mockThrowable0, mockThrowable1);
      assertNotSame(mockThrowable1, mockThrowable0);
      assertTrue(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      assertEquals("7(MpYS%InqI?=h7a", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable0.toString());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable1.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable1.toString());
      assertTrue(boolean0);
      
      boolean boolean1 = settableFuture0.setException(mockThrowable1);
      assertFalse(mockThrowable0.equals((Object)mockThrowable1));
      assertFalse(mockThrowable1.equals((Object)mockThrowable0));
      assertFalse(boolean1 == boolean0);
      assertNotSame(mockThrowable0, mockThrowable1);
      assertNotSame(mockThrowable1, mockThrowable0);
      assertTrue(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      assertEquals("7(MpYS%InqI?=h7a", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable0.toString());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable1.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable1.toString());
      assertFalse(boolean1);
      
      boolean boolean2 = settableFuture0.cancel(false);
      assertTrue(boolean2 == boolean1);
      assertFalse(boolean2 == boolean0);
      assertTrue(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      assertFalse(boolean2);
      
      SettableFuture<Object> settableFuture1 = new SettableFuture<Object>();
      assertNotNull(settableFuture1);
      assertFalse(settableFuture1.isDone());
      assertFalse(settableFuture1.isCancelled());
      
      boolean boolean3 = settableFuture1.setException(mockThrowable1);
      assertFalse(mockThrowable0.equals((Object)mockThrowable1));
      assertFalse(mockThrowable1.equals((Object)mockThrowable0));
      assertTrue(boolean3 == boolean0);
      assertFalse(boolean3 == boolean1);
      assertFalse(boolean3 == boolean2);
      assertNotSame(mockThrowable0, mockThrowable1);
      assertNotSame(mockThrowable1, mockThrowable0);
      assertEquals("7(MpYS%InqI?=h7a", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable0.toString());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable1.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable1.toString());
      assertFalse(settableFuture1.isCancelled());
      assertTrue(settableFuture1.isDone());
      assertTrue(boolean3);
      
      StackTraceElement stackTraceElement0 = new StackTraceElement("7(MpYS%InqI?=h7a", "X\rh/,", "X\rh/,", 0);
      assertNotNull(stackTraceElement0);
      assertEquals("7(MpYS%InqI?=h7a", stackTraceElement0.getClassName());
      assertEquals(0, stackTraceElement0.getLineNumber());
      assertEquals("7(MpYS%InqI?=h7a.X\rh/,(X\rh/,:0)", stackTraceElement0.toString());
      assertEquals("X\rh/,", stackTraceElement0.getFileName());
      assertEquals("X\rh/,", stackTraceElement0.getMethodName());
      assertFalse(stackTraceElement0.isNativeMethod());
      
      mockThrowable0.setOriginForDelegate(stackTraceElement0);
      assertFalse(mockThrowable0.equals((Object)mockThrowable1));
      assertNotSame(mockThrowable0, mockThrowable1);
      assertEquals("7(MpYS%InqI?=h7a", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: 7(MpYS%InqI?=h7a", mockThrowable0.toString());
      assertEquals("7(MpYS%InqI?=h7a", stackTraceElement0.getClassName());
      assertEquals(0, stackTraceElement0.getLineNumber());
      assertEquals("7(MpYS%InqI?=h7a.X\rh/,(X\rh/,:0)", stackTraceElement0.toString());
      assertEquals("X\rh/,", stackTraceElement0.getFileName());
      assertEquals("X\rh/,", stackTraceElement0.getMethodName());
      assertFalse(stackTraceElement0.isNativeMethod());
      
      SettableFuture<String> settableFuture2 = new SettableFuture<String>();
      assertNotNull(settableFuture2);
      assertFalse(settableFuture2.isCancelled());
      assertFalse(settableFuture2.isDone());
      
      TimeUnit timeUnit0 = TimeUnit.MICROSECONDS;
      try { 
        settableFuture2.get(2488L, timeUnit0);
        fail("Expecting exception: TimeoutException");
      
      } catch(TimeoutException e) {
         //
         // no message in exception (getMessage() returned null)
         //
         verifyException("org.thoughtcrime.securesms.util.concurrent.SettableFuture", e);
      }
  }

  @Test(timeout = 4000)
  public void test06()  throws Throwable  {
      SettableFuture<Integer> settableFuture0 = new SettableFuture<Integer>();
      assertNotNull(settableFuture0);
      assertFalse(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      
      MockThrowable mockThrowable0 = new MockThrowable();
      assertNotNull(mockThrowable0);
      assertNull(mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable0.toString());
      
      MockThrowable mockThrowable1 = new MockThrowable(mockThrowable0);
      assertFalse(mockThrowable1.equals((Object)mockThrowable0));
      assertNotNull(mockThrowable1);
      assertNull(mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable0.toString());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable1.toString());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable1.getMessage());
      
      mockThrowable1.addSuppressed(mockThrowable0);
      assertFalse(mockThrowable0.equals((Object)mockThrowable1));
      assertFalse(mockThrowable1.equals((Object)mockThrowable0));
      assertNotSame(mockThrowable0, mockThrowable1);
      assertNotSame(mockThrowable1, mockThrowable0);
      assertNull(mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable0.toString());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable1.toString());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable1.getMessage());
      
      boolean boolean0 = settableFuture0.setException(mockThrowable1);
      assertFalse(mockThrowable0.equals((Object)mockThrowable1));
      assertFalse(mockThrowable1.equals((Object)mockThrowable0));
      assertNotSame(mockThrowable0, mockThrowable1);
      assertNotSame(mockThrowable1, mockThrowable0);
      assertFalse(settableFuture0.isCancelled());
      assertTrue(settableFuture0.isDone());
      assertNull(mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable0.toString());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable1.toString());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable", mockThrowable1.getMessage());
      assertTrue(boolean0);
      
      TimeUnit timeUnit0 = TimeUnit.NANOSECONDS;
      try { 
        settableFuture0.get(0L, timeUnit0);
        fail("Expecting exception: ExecutionException");
      
      } catch(ExecutionException e) {
         //
         // org.evosuite.runtime.mock.java.lang.MockThrowable: org.evosuite.runtime.mock.java.lang.MockThrowable
         //
         verifyException("org.thoughtcrime.securesms.util.concurrent.SettableFuture", e);
      }
  }

  @Test(timeout = 4000)
  public void test07()  throws Throwable  {
      SettableFuture<String> settableFuture0 = new SettableFuture<String>();
      assertNotNull(settableFuture0);
      assertFalse(settableFuture0.isCancelled());
      assertFalse(settableFuture0.isDone());
      
      String string0 = "";
      MockThrowable mockThrowable0 = new MockThrowable("");
      assertNotNull(mockThrowable0);
      assertEquals("", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable0.toString());
      
      long long0 = 463L;
      TimeUnit timeUnit0 = TimeUnit.MILLISECONDS;
      // Undeclared exception!
      try { 
        mockThrowable0.printStackTrace((PrintWriter) null);
        fail("Expecting exception: NullPointerException");
      
      } catch(NullPointerException e) {
         //
         // no message in exception (getMessage() returned null)
         //
         verifyException("org.evosuite.runtime.mock.java.lang.MockThrowable", e);
      }
  }

  @Test(timeout = 4000)
  public void test08()  throws Throwable  {
      SettableFuture<Object> settableFuture0 = new SettableFuture<Object>();
      assertNotNull(settableFuture0);
      assertFalse(settableFuture0.isDone());
      assertFalse(settableFuture0.isCancelled());
      
      String string0 = "@1Myo:#r%@cV&@7w%.";
      MockThrowable mockThrowable0 = new MockThrowable("");
      assertNotNull(mockThrowable0);
      assertEquals("", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable0.toString());
      
      MockThrowable mockThrowable1 = new MockThrowable(mockThrowable0);
      assertFalse(mockThrowable1.equals((Object)mockThrowable0));
      assertNotNull(mockThrowable1);
      assertEquals("", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable0.toString());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable1.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable1.toString());
      
      MockThrowable mockThrowable2 = new MockThrowable("@1Myo:#r%@cV&@7w%.", mockThrowable1);
      assertFalse(mockThrowable0.equals((Object)mockThrowable1));
      assertFalse(mockThrowable1.equals((Object)mockThrowable0));
      assertFalse(mockThrowable2.equals((Object)mockThrowable1));
      assertFalse(mockThrowable2.equals((Object)mockThrowable0));
      assertNotNull(mockThrowable2);
      assertEquals("", mockThrowable0.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable0.toString());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable1.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: org.evosuite.runtime.mock.java.lang.MockThrowable: ", mockThrowable1.toString());
      assertEquals("@1Myo:#r%@cV&@7w%.", mockThrowable2.getMessage());
      assertEquals("org.evosuite.runtime.mock.java.lang.MockThrowable: @1Myo:#r%@cV&@7w%.", mockThrowable2.toString());
      
      MockPrintWriter mockPrintWriter0 = null;
      try {
        mockPrintWriter0 = new MockPrintWriter("", "");
        fail("Expecting exception: UnsupportedEncodingException");
      
      } catch(Throwable e) {
         //
         // 
         //
         verifyException("org.evosuite.runtime.mock.java.io.MockPrintWriter", e);
      }
  }

  @Test(timeout = 4000)
  public void test09()  throws Throwable  {
      SettableFuture<Object> settableFuture0 = new SettableFuture<Object>();
      assertNotNull(settableFuture0);
      assertFalse(settableFuture0.isCancelled());
      assertFalse(settableFuture0.isDone());
      
      boolean boolean0 = settableFuture0.cancel(true);
      assertTrue(settableFuture0.isCancelled());
      assertFalse(settableFuture0.isDone());
      assertTrue(boolean0);
      
      TimeUnit timeUnit0 = TimeUnit.MICROSECONDS;
      try { 
        settableFuture0.get(1559L, timeUnit0);
        fail("Expecting exception: TimeoutException");
      
      } catch(TimeoutException e) {
         //
         // no message in exception (getMessage() returned null)
         //
         verifyException("org.thoughtcrime.securesms.util.concurrent.SettableFuture", e);
      }
  }

  @Test(timeout = 4000)
  public void test10()  throws Throwable  {
      SettableFuture<String> settableFuture0 = new SettableFuture<String>();
      settableFuture0.isCancelled();
      settableFuture0.cancel(false);
      SettableFuture<Object> settableFuture1 = new SettableFuture<Object>();
      settableFuture1.cancel(true);
      settableFuture1.set(settableFuture0);
      settableFuture1.cancel(false);
      MockThrowable mockThrowable0 = new MockThrowable();
      mockThrowable0.getStackTrace();
      settableFuture1.setException(mockThrowable0);
      TimeUnit timeUnit0 = TimeUnit.NANOSECONDS;
      try { 
        settableFuture1.get((-2943L), timeUnit0);
        fail("Expecting exception: TimeoutException");
      
      } catch(TimeoutException e) {
         //
         // no message in exception (getMessage() returned null)
         //
         verifyException("org.thoughtcrime.securesms.util.concurrent.SettableFuture", e);
      }
  }

  @Test(timeout = 4000)
  public void test11()  throws Throwable  {
      SettableFuture<Integer> settableFuture0 = new SettableFuture<Integer>();
      SettableFuture<String> settableFuture1 = new SettableFuture<String>();
      settableFuture1.cancel(true);
      SettableFuture<Object> settableFuture2 = new SettableFuture<Object>();
      settableFuture2.set(settableFuture1);
      settableFuture1.isCancelled();
      settableFuture1.set("");
      MockThrowable mockThrowable0 = new MockThrowable("");
      MockThrowable mockThrowable1 = new MockThrowable("", mockThrowable0);
      MockThrowable mockThrowable2 = new MockThrowable(mockThrowable1);
      mockThrowable1.addSuppressed(mockThrowable0);
      // Undeclared exception!
      try { 
        mockThrowable1.initCause(mockThrowable0);
        fail("Expecting exception: IllegalStateException");
      
      } catch(IllegalStateException e) {
         //
         // Can't overwrite cause with org.evosuite.runtime.mock.java.lang.MockThrowable: 
         //
         verifyException("org.evosuite.runtime.mock.java.lang.MockThrowable", e);
      }
  }
}
